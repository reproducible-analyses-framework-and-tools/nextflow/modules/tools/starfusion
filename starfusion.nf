#!/usr/bin/env Nextflow

process starfusion {
// require:
//   FQS
//   params.starfusion$ctat_ref
//   params.starfusion$starfusion_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'starfusion_container'
  label 'starfusion'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/starfusion"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(fq1), path(fq2)
  path ctat_ref
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.abridged.coding_effect*"), optional: true, emit: coding_effect_fusions
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.tsv"), emit: full_fusions
  tuple val(pat_name), val(run), val(dataset), path("*bam"), emit: bams

  script:
  """
  /usr/local/src/STAR-Fusion/STAR-Fusion --CPU ${task.cpus} --genome_lib_dir /\${PWD}/${ctat_ref} ${parstr} --left_fq ${fq1} --right_fq ${fq2} --output_dir .

  for i in star-fusion.fusion_predictions.tsv star-fusion.fusion_predictions.abridged.tsv star-fusion.fusion_predictions.abridged.coding_effect.tsv; do
    [ ! -f \${i} ] || mv  \${i} ${dataset}-${pat_name}-${run}.\${i}
  done

  rm -rf star-fusion.preliminary
  """
}


process jstarfusion {
// require:
//   JUNCTIONS
//   params.starfusion$ctat_ref
//   params.starfusion$starfusion_parameters

  tag "${dataset}/${pat_name}/${run}"
  label 'starfusion_container'
  label 'starfusion'
  publishDir "${params.samps_out_dir}/${dataset}/${pat_name}/${run}/starfusion"
  cache 'lenient'

  input:
  tuple val(pat_name), val(run), val(dataset), path(junctions)
  path ctat_ref
  val parstr

  output:
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.abridged.coding_effect*"), optional: true, emit: coding_effect_fusions
  tuple val(pat_name), val(run), val(dataset), path("${dataset}*fusion_predictions.tsv"), emit: full_fusions

  script:
  """
  /usr/local/src/STAR-Fusion/STAR-Fusion --CPU ${task.cpus} --genome_lib_dir \${PWD}/${ctat_ref} ${parstr} -J ${junctions}  --output_dir .

  for i in star-fusion.fusion_predictions.tsv star-fusion.fusion_predictions.abridged.tsv star-fusion.fusion_predictions.abridged.coding_effect.tsv; do
    [ ! -f \${i} ] || mv  \${i} ${dataset}-${pat_name}-${run}.\${i}
  done

  rm -rf star-fusion.preliminary
  """
}
